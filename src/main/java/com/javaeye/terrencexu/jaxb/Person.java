package com.javaeye.terrencexu.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

public class Person {

    @XmlElement(name="IdValue")
    private IdValue idValue;
    
    @XmlElement(name="name")
    private String name;
    
    @XmlElement(name="age")
    private int age;
    
    @XmlElement(name="address")
    private String address;

    public Person() {
    }
    
    public Person(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }
    
    @XmlTransient
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @XmlTransient
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @XmlTransient
    public IdValue getIdValue() {
        return idValue;
    }

    public void setIdValue(IdValue idValue) {
        this.idValue = idValue;
    }
    
}
