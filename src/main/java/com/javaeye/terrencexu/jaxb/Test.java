package com.javaeye.terrencexu.jaxb;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import com.yung.tool.ConsoleDebugPrinter;

public class Test {
	
	public static void main(String[] args) throws FileNotFoundException {
		//testUnmarshal();
		//testMarshal();
	    testMarshal2();
	}

	public static void testUnmarshal() throws FileNotFoundException { 
		InputStream in = Test.class.getClassLoader().getResourceAsStream("employees.xml");
		Employees employees = (Employees) XMLParser.unmarshal( in, Employees.class);
		List<Employee> employeeList = employees.getEmployees();
		ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
		printer.printObjectParam("employeeList", employeeList);
	}

	public static void testMarshal() {
		Employees employees = new Employees();
		employees.addEmployee(new Employee("johnsmith@company.com", "abc123_", "John Smith", 24, Gender.MALE));
		employees.addEmployee(new Employee("christinechen@company.com", "123456", "Christine Chen", 27, Gender.FEMALE));
		String result = XMLParser.marshal(employees, Employees.class);
		System.out.println(result);
	}
	
	public static void testMarshal2() {
        Contacts contacts = new Contacts();
        Person person = new Person("123", 10, "abc");
        person.setIdValue(new IdValue("employeeId", "111222"));
        contacts.getPersons().add(person);
        String result = XMLParser.marshal(contacts, Contacts.class);
        System.out.println(result);
    }
}
